from flask import Flask, render_template, request, redirect, url_for
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/users/<username>')
def show_user(username):
    return render_template('profile.html', user=username)

# membuat route halaman login
@app.route('/login', methods=['GET', 'POST'])
def get_login():
    # mengecek method yang digunakan
    if request.method == 'POST':
        return redirect(url_for('show_user', username=request.form['username']))

    return render_template('login.html')